This Repository contains all the required material to build the browser on Linux platforms. 

### Requirements
* Ubuntu or Linux Mint (Bionic or later)

### Notes
* As this script installs files during the build, it is recommended to run this script in a VM or installation dedicated to building. 
* You will occassionally need to enter your sudo password. Running this script as root has not been tested
* This script has been tested with Linux Mint 19.1
* The flatpak build is not ready yet.

### Instructions
* Run build.sh inside this folder. 
* Packages will be saved in the repository root folder. 
